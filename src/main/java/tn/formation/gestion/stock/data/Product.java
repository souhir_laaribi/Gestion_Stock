package tn.formation.gestion.stock.data;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Product {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private Long reference;
	private String label;
	private Double price;
	private String description;
	private Double width;
	private Double length;
	private Double height;
	private Integer quantity;
	
	@Enumerated
    private PackagingType packaging;
	
	@JoinColumn(name = "arrival_id", referencedColumnName = "id")
	@ManyToOne(optional = false)
    private Arrival arrivalId;
	
	@ManyToMany
    @JoinTable(name = "Warehouse_Product", joinColumns = { @JoinColumn(name = "Product_id", referencedColumnName = "id") },
    	      inverseJoinColumns = { @JoinColumn(name = "Warehouse_id", referencedColumnName = "id") })
    private List<Warehouse> warehouses;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getReference() {
		return reference;
	}

	public void setReference(Long reference) {
		this.reference = reference;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getWidth() {
		return width;
	}

	public void setWidth(Double width) {
		this.width = width;
	}

	public Double getLength() {
		return length;
	}

	public void setLength(Double length) {
		this.length = length;
	}

	public Double getHeight() {
		return height;
	}

	public void setHeight(Double height) {
		this.height = height;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public PackagingType getPackaging() {
		return packaging;
	}

	public void setPackaging(PackagingType packaging) {
		this.packaging = packaging;
	}

	public Arrival getArrivalId() {
		return arrivalId;
	}

	public void setArrivalId(Arrival arrivalId) {
		this.arrivalId = arrivalId;
	}

	public List<Warehouse> getWarehouses() {
		return warehouses;
	}

	public void setWarehouses(List<Warehouse> warehouses) {
		this.warehouses = warehouses;
	}
	
}
